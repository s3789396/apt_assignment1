#include <iostream>
#include <fstream>
#include <stdexcept>
#include <string>

#include "Types.h"
#include "Node.h"
#include "NodeList.h"
#include "PathPlanner.h"

//number of rows and columns of the env
const int rows = ENV_DIM;
const int cols = ENV_DIM;

// Helper test functions
void testNode();
void testNodeList();

// Read a environment from standard input.
void readEnvStdin(Env env);

Env make_env(const int rows, const int cols);

// Print out a Environment to standard output with path.
// To be implemented for Milestone 3
void printPath(Env env, NodeList* solution);


int main(int argc, char** argv){
    // THESE ARE SOME EXAMPLE FUNCTIONS TO HELP TEST YOUR CODE
    // AS YOU WORK ON MILESTONE 2. YOU CAN UPDATE THEM YOURSELF
    // AS YOU GO ALONG.
    // COMMENT THESE OUT BEFORE YOU SUBMIT!!!
    // std::cout << "TESTING - COMMENT THE OUT TESTING BEFORE YOU SUBMIT!!!" << std::endl;
    // testNode();
    // testNodeList();
    // std::cout << "DONE TESTING" << std::endl << std::endl;

    // Load Environment
    //milestone 4, create env's size of choise
    Env env = make_env(rows ,cols);
    readEnvStdin(env);

    // Solve using forwardSearch
    // THIS WILL ONLY WORK IF YOU'VE FINISHED MILESTONE 2
    PathPlanner* pathplanner = new PathPlanner(env, rows, cols);
    NodeList* reachablePositions = nullptr;
    reachablePositions = pathplanner->getReachableNodes();

    // Get the path
    // THIS WILL ONLY WORK IF YOU'VE FINISHED MILESTONE 3
    NodeList* solution = pathplanner->getPath(reachablePositions);

    //print the path
    printPath(env, solution);

    //delete of the env have done in pathplanner
    delete pathplanner;
    delete reachablePositions;
    delete solution;

}

Env make_env(const int rows, const int cols) {
   Env env = nullptr;

   if (rows >= 0 && cols >= 0) {
      env = new char*[rows];
      for (int i = 0; i != rows; ++i) {
         env[i] = new char[cols];
      }
   }

   return env;
}

void readEnvStdin(Env env){
    //TODO 
    //i use string instead of char to read 1 line as a string.
    std::string c;
    for(int row = 0; row < rows; ++row) {
        std::cin >> c;
        for(int col = 0; col < cols; ++col){
           env[row][col] = c[col];
        }
   }
}

void printPath(Env env, NodeList* solution) {
    //TODO
    NodePtr node;//the previous node
    NodePtr nextNode;//the node that need to change
    for(int i = 1; i < solution->getLength() - 1; ++i){//the start and goal node dont need to change
        node = solution->get(i - 1);                                    
        nextNode = solution->get(i);

    //change the empty space into arrow
        //find up position
        if(node->getRow() - 1 == nextNode->getRow() ){
            env[nextNode->getRow()][nextNode->getCol()] = MOVE_DOWN;
        }

        //find right position
        if(node->getCol() + 1 == nextNode->getCol() ){
            env[nextNode->getRow()][nextNode->getCol()] = MOVE_LEFT;
        }

        //find down position
        if(node->getRow() + 1 == nextNode->getRow() ){
            env[nextNode->getRow()][nextNode->getCol()] = MOVE_UP;
        }

        //find left position
        if(node->getCol() - 1 == nextNode->getCol() ){
            env[nextNode->getRow()][nextNode->getCol()] = MOVE_RIGHT;
        }
    }
    
    //print out environment with solution
    for(int r = 0; r < rows; ++r) {
       for(int c = 0; c < cols; ++c){
           std::cout << env[r][c];
           if(c == cols - 1) std::cout << std::endl;
       }
   }
}

void printReachablePositions(Env env, NodeList* reachablePositions){
    //TODO
    //I create a new test.cpp to test so this has nothing
}

void testNode() {
    std::cout << "TESTING Node" << std::endl;

    // Make a Node and print out the contents
    Node* node = new Node(1, 1, 2);
    std::cout << node->getRow() << ",";
    std::cout << node->getCol() << ",";
    std::cout << node->getDistanceToS() << std::endl;
    delete node;

    // Change Node and print again
    node = new Node(4, 2, 3);
    std::cout << node->getRow() << ",";
    std::cout << node->getCol() << ",";
    std::cout << node->getDistanceToS() << std::endl;
    delete node;
}

void testNodeList() {
    std::cout << "TESTING NodeList" << std::endl;

    // Make a simple NodeList, should be empty size
    NodeList* nodeList = new NodeList();
    std::cout << "NodeList size: " << nodeList->getLength() << std::endl;

    // Add a Node to the NodeList, print size
    Node* b1 = new Node(1, 1, 1);
    nodeList->addBack(b1);
    std::cout << "NodeList size: " << nodeList->getLength() << std::endl;

    // Add second Nodetest
    Node* b2 = new Node(0, 0, 1);
    nodeList->addBack(b2);
    std::cout << "NodeList size: " << nodeList->getLength() << std::endl;

    // Test Get-ith - should be 0,0,1
    Node* getB = nodeList->get(1);
    std::cout << getB->getRow() << ",";
    std::cout << getB->getCol() << ",";
    std::cout << getB->getDistanceToS() << std::endl;

    // Print out the NodeList
    std::cout << "PRINTING OUT A NODELIST IS AN EXERCISE FOR YOU TO DO" << std::endl;
}