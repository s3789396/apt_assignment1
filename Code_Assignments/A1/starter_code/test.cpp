#include <iostream>
#include <fstream>
#include <stdexcept>
#include <string>

#include "Types.h"
#include "Node.h"
#include "NodeList.h"
#include "PathPlanner.h"

//g++ -Wall -Werror -std=c++14 -O -o red7 w01-10-red7.cpp

void read(Env env);
void testDelete();

int main(void) {

   testDelete();
   Env env;
   read(env);

   for(int r = 0; r < ENV_DIM; ++r) {
       for(int c = 0; c < ENV_DIM; ++c){
           std::cout << env[r][c];
           if(c == 19) std::cout << std::endl;
       }
   }

//===========================================

   PathPlanner* pp = new PathPlanner(env, rows, cols);
   
   NodeList* nl = pp->getReachableNodes();

   for(int i = 0; i < nl->getLength(); ++i) {
      std::cout << nl->get(i)->getRow()<< " " << nl->get(i)->getCol() << " " << nl->get(i)->getDistanceToS() << std::endl;
   }

   std::cout << std::endl;

   nl = pp->getPath(nl);

   for(int i = 0; i < nl->getLength(); ++i) {
      std::cout << nl->get(i)->getRow()<< " " << nl->get(i)->getCol() << " " << nl->get(i)->getDistanceToS() << std::endl;
   }
   
   return EXIT_SUCCESS;
}
















void read(Env env){
    //TODO 
    std::string a;
    for(int r = 0; r < ENV_DIM; ++r) {
        std::cin >> a;
        for(int c = 0; c < ENV_DIM; ++c){
           env[r][c] = a[c];
        }
   }
}

void testDelete() {
   NodePtr p = nullptr;
    p = new Node(5,6,7);
   std::cout << p->getCol()  << std::endl;
   p->setDistanceToS(41);
   
   std::cout << p->getDistanceToS() << std::endl;

   NodeList* nl = nullptr;
   nl = new NodeList();
   nl->addBack(p);
   p = new Node(8,9,10);
   nl->addBack(p);
   NodePtr q = new Node(5,6,7);

   NodeList* reachablePositions = nullptr;

   reachablePositions = nl;

   

   std::cout << reachablePositions->get(0)->getDistanceToS() << std::endl;
   std::cout << reachablePositions->get(1)->getDistanceToS() << std::endl;

   std::cout << reachablePositions->containsNode(q) << std::endl;
   q = nl->get(1);

   
   std::cout << q->getDistanceToS() << std::endl;

   delete nl;
   // std::cout << p->getDistanceToS() << std::endl;
    
   // std::cout << nl->get(1)->getCol() << std::endl;
}