
#include "PathPlanner.h"

#include <iostream>


PathPlanner::PathPlanner(Env env, int rows, int cols){
    this->rows = rows;
    this->cols = cols;
    this->env = env;
}

PathPlanner::~PathPlanner(){
   delete S;
   if (rows >= 0 && cols >= 0) {
        for (int i = 0; i != rows; ++i) {
            delete env[i];
        }
        delete env;
   }
}

void PathPlanner::initialPosition(int row, int col){
    this->S = new Node(row,col,0);
}

NodeList* PathPlanner::getReachableNodes(){
    //get start position
    for(int r = 0; r < this->rows; ++r) {
        for(int c = 0; c < this->cols; ++c){
            if(this->env[r][c] == SYMBOL_START)
                initialPosition(r,c);
        }
    }
    //initialize open and close nodelist
    NodeList* O = new NodeList();
    NodeList* C = new NodeList();
    NodePtr p = new Node(S->getRow(),S->getCol(),S->getDistanceToS());
    O->addBack(p);

    bool exit = false; 
    NodePtr q;
    int pRow;
    int pCol;
    do{
        

        pRow = p->getRow();
        pCol = p->getCol();

        // up position
        if(this->env[pRow - 1][pCol] == SYMBOL_EMPTY || this->env[pRow - 1][pCol] == SYMBOL_GOAL) {
            q = new Node(pRow - 1, pCol, p->getDistanceToS()+1);
            if( !(O->containsNode(q)) ) {
                O->addBack(q);
            }else delete q;
        }

        // right position
        if(this->env[pRow][pCol + 1] == SYMBOL_EMPTY || this->env[pRow][pCol + 1] == SYMBOL_GOAL) {
            q = new Node(pRow, pCol + 1, p->getDistanceToS()+1);
            if( !(O->containsNode(q)) ) {
                O->addBack(q);
            }else delete q;
        }

        // down position
        if(this->env[pRow + 1][pCol] == SYMBOL_EMPTY || this->env[pRow + 1][pCol] == SYMBOL_GOAL) {
            q = new Node(pRow + 1, pCol, p->getDistanceToS()+1);
            if( !(O->containsNode(q)) ) {
                O->addBack(q);
            }else delete q;
        }

        // left position
        if(this->env[pRow][pCol - 1] == SYMBOL_EMPTY || this->env[pRow][pCol - 1] == SYMBOL_GOAL) {
            q = new Node(pRow, pCol - 1, p->getDistanceToS()+1);
            if( !(O->containsNode(q)) ) {
                O->addBack(q);
            }else delete q;
        }
        p = new Node(p->getRow(), p->getCol(), p->getDistanceToS());
        C->addBack(p);

        
        for(int i = 0; C->containsNode(p) && i < O->getLength(); ++i) {
            p = O->get(i);
        }

        if(C->containsNode(p)) exit = true;
    } while(!exit);

    delete C;

    return O;
}

NodeList* PathPlanner::getPath(NodeList* O){
    NodeList* nl = new NodeList();
    NodePtr p = nullptr;
    // get goal position
    int goalRow = 0;
    int goalCol = 0;
    for(int r = 0; r < this->rows; ++r) {
        for(int c = 0; c < this->cols; ++c){
            if(this->env[r][c] == SYMBOL_GOAL){
                goalRow = r;
                goalCol = c;
            }
        }
    }

    //get goal node from reachable nodelist
    for(int i = 0; i < O->getLength(); ++i) {
        if(O->get(i)->getRow() == goalRow && O->get(i)->getCol() == goalCol){
            p = new Node(goalRow, goalCol, O->get(i)->getDistanceToS());
            nl->addBack(p);
        }
    }

    // get path
    do{
        for(int i = 0; i < O->getLength(); ++i) {
            // up position
            if(O->get(i)->getRow() == p->getRow() - 1 && O->get(i)->getCol() == p->getCol()) {
                if(O->get(i)->getDistanceToS() == p->getDistanceToS() - 1){
                    p = O->get(i);
                }
            }

            // right position
            if(O->get(i)->getRow() == p->getRow() && O->get(i)->getCol() == p->getCol() + 1) {
                if(O->get(i)->getDistanceToS() == p->getDistanceToS() - 1){
                    p = O->get(i);
                }
            }

            // down position
            if(O->get(i)->getRow() == p->getRow() + 1 && O->get(i)->getCol() == p->getCol()) {
                if(O->get(i)->getDistanceToS() == p->getDistanceToS() - 1){
                    p = O->get(i);
                }
            }

            // left position
            if(O->get(i)->getRow() == p->getRow() && O->get(i)->getCol() == p->getCol() - 1) {
                if(O->get(i)->getDistanceToS() == p->getDistanceToS() - 1){
                    p = O->get(i);
                }
            }
        }

        p = new Node(p->getRow(), p->getCol(), p->getDistanceToS());
        nl->addBack(p);
    }while(!nl->containsNode(S));

    return nl;
}
