
#include "NodeList.h"
#include <iostream>



NodeList::NodeList(){
   this->length = 0;
}


NodeList::~NodeList(){
   for(int i = 0; i < length; ++i){
      delete nodes[i];
   }
}


int NodeList::getLength(){
   return this->length;
}


NodePtr NodeList::get(int i){
   return nodes[i];
}

void NodeList::addBack(NodePtr newNode){
   //create another node list with the size 1 more than nodes 
   NodePtr* another = nullptr;
   another = new NodePtr[length + 1];

   //add all the node in current node list to another
   for (int i = 0; i < length; ++i){
      another[i] = nodes[i];
   }

   //add the new node to another
   another[length] = newNode;

   //add another to the main nodes list
   nodes = another;

   //expand length
   this->length += 1;
}

bool NodeList::containsNode(NodePtr node){
   for(int i = 0; i < length; ++i){
      //check if the node list contain node with the same col and row
      if(nodes[i]->getRow() == node->getRow() && nodes[i]->getCol() == node->getCol()) return true;
   } 
   return false;
}

void NodeList::clear(){
   
}